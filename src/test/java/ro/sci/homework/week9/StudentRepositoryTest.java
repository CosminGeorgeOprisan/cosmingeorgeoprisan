package ro.sci.homework.week9;

import org.junit.jupiter.api.Test;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class StudentRepositoryTest {
    StudentRepository studentList = new StudentRepository();

    @Test
    void addStudent_incorrectGender_failToAdd() {
        int initialSize = studentList.getStudentsList().size();
        studentList.addStudent("Cosmin", "Popescu",
                LocalDate.of(2002, 12, 10), "Mali", "1234");
        assertEquals(initialSize, studentList.getStudentsList().size());
    }

    @Test
    void addStudent_correctGender_successToAdd() {
        int initialSize = studentList.getStudentsList().size();
        studentList.addStudent("Cosmin", "Popescu",
                LocalDate.of(2002, 12, 10), "Male", "1234");
        assertFalse(initialSize == studentList.getStudentsList().size());
    }

    @Test
    void deleteStudent_incorrectCnp_sendException() {
        Exception exception = assertThrows(CustomValidationException.class, () -> {
            studentList.deleteStudent("456");
        });
        String expectedMessage = "Invalid CNP, does not exists a student with the provided CNP in the list";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void deleteStudent_correctCnp_deleteSuccess() {
       studentList.addStudent("Cosmin", "Popescu",
               LocalDate.of(2002, 12, 10), "Male", "1234");
       studentList.addStudent("Cosmin", "Popescu",
               LocalDate.of(2002, 12, 10), "Male", "23455");

       int sizeAfterAdding = studentList.getStudentsList().size();

       studentList.deleteStudent("1234");

       assertEquals(1, studentList.getStudentsList().size());
    }

    @Test
    void aPersonOfInvalidOld_retrieveStudentsOfAge_sendException() {
        Exception exception = assertThrows(CustomValidationException.class, () -> {
            studentList.retrieveStudentsOfAge(-2);
        });
        String expectedMessage = "Age entered is invalid, must be greater than 0";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);

    }
}