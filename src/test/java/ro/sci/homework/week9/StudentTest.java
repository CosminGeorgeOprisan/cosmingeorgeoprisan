package ro.sci.homework.week9;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class StudentTest {

    @Test
    public void tryToCreateAStudent_invalidFirstName_sendException() {

        Exception exception = assertThrows(CustomValidationException.class, () -> {
            new Student("", "Popescu", LocalDate.of(1996, 1, 10),
                    "Male", "1234");
        });

        String expectedMessage = "First name is empty";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    public void tryToCreateAStudent_invalidLastName_sendException() {

        Exception exception = assertThrows(CustomValidationException.class, () -> {
            new Student("Cosmin", "", LocalDate.of(1996, 1, 10),
                    "Male", "1234");
        });

        String expectedMessage = "Last name is empty";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);

    }


    @Test
    public void tryToCreateAStudent_tooYoung_sendException() {

        Exception exception = assertThrows(CustomValidationException.class, () -> {
            new Student("Cosmin", "Popescu", LocalDate.of(2010, 1, 10),
                    "Male", "1234");
        });

        String expectedMessage = "Invalid date of birth entered, age must be at least 18 and under 122";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);

    }
    @Test
    public void tryToCreateAStudent_tooOld_sendException() {

        Exception exception = assertThrows(CustomValidationException.class, () -> {
            new Student("Cosmin", "Popescu", LocalDate.of(1896, 1, 10),
                    "Male", "1234");
        });

        String expectedMessage = "Invalid date of birth entered, age must be at least 18 and under 122";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);

    }
}