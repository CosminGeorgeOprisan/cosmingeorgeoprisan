package ro.sci.homework.week12;

import org.junit.jupiter.api.Test;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.concurrent.ThreadLocalRandom;

public class AppTest {

    @Test

    public void test_delete_a_row() {

        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Homework",
                "postgres", "Eminem123@")) {
            Statement deleteRow1 = connection.createStatement();
            deleteRow1.executeUpdate("DELETE FROM accommodation_room_fair_relation");
            deleteRow1.executeUpdate("DELETE FROM accommodation");
            deleteRow1.executeUpdate("DELETE FROM room_fair");
        } catch (SQLException s) {
            s.printStackTrace();
        }
    }

    @Test

    public void test_insert_statement() {
        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Homework",
                "postgres", "Eminem123@")) {

            PreparedStatement preparedStatement1 = connection.prepareStatement("INSERT INTO accommodation " +
                    "(id, type, bed_type, max_guests, description) values (?,?,?,?,?)");
            PreparedStatement preparedStatement2 = connection.prepareStatement("INSERT INTO room_fair " +
                    "(id, value, season) values(?,?,?)");
            PreparedStatement preparedStatement3 = connection.prepareStatement("INSERT INTO accommodation_room_fair_relation " +
                    "(id, accommodation_id, room_fair_id) values (?,?,?)");

            for (int i = 1; i <= 10; i++) {
                preparedStatement1.setInt(1, i);
                int randomIDRoomFair = ThreadLocalRandom.current().nextInt(10000);
                preparedStatement2.setInt(1, randomIDRoomFair);
                preparedStatement3.setInt(1, i);

                preparedStatement1.setString(2, RoomType.getRandom().toString());
                double randomRoomFair = ThreadLocalRandom.current().nextDouble(245, 700);
                preparedStatement2.setDouble(2, Double.parseDouble(new DecimalFormat("#.##")
                        .format(randomRoomFair)));
                preparedStatement3.setInt(2, i);

                preparedStatement1.setString(3, BedType.getRandom().toString());
                preparedStatement2.setString(3, Season.getRandom().toString());
                preparedStatement3.setInt(3, randomIDRoomFair);

                preparedStatement1.setInt(4, 2);

                preparedStatement1.setString(5, "this room has a great view");

                preparedStatement1.executeUpdate();
                preparedStatement2.executeUpdate();
                preparedStatement3.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test

    public void test_join_tables() {
        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Homework",
                "postgres", "Eminem123@")) {
            Statement joinTables = connection.createStatement();
            ResultSet resultSet = joinTables.executeQuery("SELECT a.id as RoomNo, r.value as price from " +
                    "accommodation_room_fair_relation a join room_fair r on a.room_fair_id = r.id");
            while (resultSet.next()) {
                System.out.println(resultSet.getString("RoomNo") + " | ");
                System.out.println(resultSet.getString("Price") + " RON ");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test

    public void test_connectivity_DB() {

        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Homework",
                "postgres", "Eminem123@")) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM accommodation");
            while (resultSet.next()) {
                System.out.println(resultSet.getInt(1) + " | ");
                System.out.println(resultSet.getString(2) + " | ");
                System.out.println(resultSet.getString(3) + " | ");
                System.out.println(resultSet.getInt(4) + " | ");
                System.out.println(resultSet.getString(5));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test

    public void test_load_driver() {
        try {
            Class.forName("org.postgresql.Driver").newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            System.err.println("Cannot load the driver, verify classpath");
            System.err.println(e.getMessage());
        }
    }
}
