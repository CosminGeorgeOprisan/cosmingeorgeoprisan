package ro.sci.homework.week6;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class CalculatorTest {

    Calculator calculator = new Calculator();

    @Test
    public void differentScalesExpression_calculateAddition_62_0milliMeters() {

        String measureUnit = "mm";

        assertEquals("62.0 mm", calculator.calculate("5 cm + 12 mm", measureUnit));
    }

    @Test
    public void differentScalesExpression_calculateSubtraction_9_2deciMeters() {

        String measureUnit = "dm";

        assertEquals("9.2 dm", calculator.calculate("10 dm - 8 cm", measureUnit));
    }

    @Test
    public void differentScalesExpression_mixedOperations_1_7897kiloMeters() {

        String measureUnit = "km";
        assertEquals("1.7897 km", calculator.calculate("2 km - 230 m + 200 dm - 30 cm", measureUnit));
    }

    @Test
    public void expressionWithInvalidOperands_checkConditions_exception() {

        String measureUnit = "cm";

        assertEquals("Found invalid operands in the expression",
                calculator.calculate("4 cm * 6 mm / 5 m", measureUnit));
    }

    @ParameterizedTest
    @ValueSource(strings = {"m"})
    public void differentScalesExpression_mixedOperations_1789_7meters(String measureUnit) {

        assertEquals("1789.7 m", calculator.calculate("2 km - 230 m + 200 dm - 30 cm", measureUnit));
    }

}
