package ro.sci.homework.week6;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;
import org.junit.platform.suite.api.SuiteDisplayName;

@Suite
@SuiteDisplayName("Our tests")
@SelectClasses({CalculatorTest.class})
public class TestSuite {

}

