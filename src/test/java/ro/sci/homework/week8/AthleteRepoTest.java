package ro.sci.homework.week8;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AthleteRepoTest {

    List<Athlete> athleteList;
    Athlete cosmin = new Athlete(1, "cosmin", "RO",
            "29:50", "xxxxx", "xxxxx", "xooox");
    Athlete bogdan = new Athlete(2, "bogdan", "POR",
            "28:35", "xoxox", "xoxxx", "xxoxx");

    @BeforeEach
    void setUp() {
        athleteList = new ArrayList<>();
        athleteList.add(cosmin);
        athleteList.add(bogdan);
    }

    @Test
    void nonExistentFile_readFromFileToList_fileNotFound() {
        Exception exception = assertThrows(FileNotFoundException.class, () -> {
            AthleteRepo.readFromFileToList("OtherFile.csv");
        });
        String expectedMessage = "The system cannot find the file specified";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void existentFile_readFromFileToList_listGotElements() throws IOException {
        List list = AthleteRepo.readFromFileToList("C:\\Users\\40726\\IdeaProjects\\CosminGeorgeOprisan\\Test.csv");
        assertFalse(list.isEmpty());
    }

    @Test
    void initialTimeValue_calculateTime_30_20() {

        AthleteRepo.calculateTime(athleteList);
        String testString = "30:20";
        assertEquals(testString, cosmin.getTime());
    }

    @Test
    void athleteList_showStandings_athlete2First() {

        AthleteRepo.calculateTime(athleteList);
        Athlete athlete = AthleteRepo.showStandings(athleteList);
        assertSame(bogdan, athlete);
    }

    @Test
    void differentTimes_comparator_differentPlaces() {
        AthleteComparator comparator = new AthleteComparator();
        AthleteRepo.calculateTime(athleteList);
        int value = comparator.compare(athleteList.get(0), athleteList.get(1));
        assertTrue(value > 0);

    }
}