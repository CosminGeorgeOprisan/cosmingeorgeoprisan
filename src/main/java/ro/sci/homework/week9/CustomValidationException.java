package ro.sci.homework.week9;

public class CustomValidationException extends RuntimeException{

    public CustomValidationException(String message) {
        super(message);
    }
}
