package ro.sci.homework.week9;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {

        StudentRepository studentRepository = new StudentRepository();

        System.out.println("added students");

        studentRepository.addStudent("Cosmin", "Oprisan",
                LocalDate.of(2004, 1, 10), "Male", "12346");

        studentRepository.addStudent("Bogdan", "Comanescu",
                LocalDate.of(1992, 5, 1), "Male", "12345");

        studentRepository.orderStudentsByLastName();

        studentRepository.getStudentsList().forEach(System.out::println);

        System.out.println("retrieved students");
        studentRepository.retrieveStudentsOfAge(18);

        System.out.println("remained students");
        studentRepository.deleteStudent("12345");

        studentRepository.getStudentsList().forEach(System.out::println);

    }

}
