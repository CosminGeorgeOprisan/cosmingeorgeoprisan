package ro.sci.homework.week9;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StudentRepository {
    private static Logger logger = Logger.getLogger(StudentRepository.class.getName());
    List<Student> studentsList = new ArrayList<>();

    //Verify if we have all conditions valid to can add a student in our list,if not throw an exception
    public void addStudent(String firstName, String lastName, LocalDate dateOfBirth, String gender, String cnp) {

        try {
            studentsList.add(new Student(firstName, lastName, dateOfBirth, gender, cnp));
        } catch (CustomValidationException ex) {
            logger.log(Level.SEVERE, "Validation error while trying to create a new student", ex);
        }
    }

    //Try to delete a student from our list based on his CNP,if we cannot find it throw an exception
    public void deleteStudent(String cnp) throws CustomValidationException {
        if (!cnpValidation(cnp)) {
            throw new CustomValidationException("Invalid CNP, does not exists a student with the provided CNP in the list");
        }

        for (int i = 0; i < studentsList.size(); i++) {
            if (studentsList.get(i).getCnp().equals(cnp)) {
                studentsList.remove(studentsList.get(i));
                i--;
            }
        }
    }

    //Get all persons from the list of a specified age based on date of birth and local date
    //If the age parameter is invalid throw an exception
    public void retrieveStudentsOfAge(int age) throws CustomValidationException {
        if (age < 0) {
            throw new CustomValidationException("Age entered is invalid, must be greater than 0");
        }
        for (Student student : studentsList) {
            if (getAge(student.getDateOfBirth()) == age) {
                System.out.println(student.toString());
            }
        }
    }

    //Order our students based on lastName
    //No need validation for the last name attribute because we already trigger an exception if lastName is empty
    public void orderStudentsByLastName() {
        studentsList.sort(Comparator.comparing(Student::getLastName));
    }


    public List<Student> getStudentsList() {
        return studentsList;
    }

    private boolean cnpValidation(String cnp) {
        if (cnp == null || cnp.trim().equals("")) {
            return false;
        }

        for (Student student : studentsList) {
            if (cnp.equals(student.getCnp())) {
                return true;
            }
        }
        return false;
    }

    private int getAge(LocalDate dateOfBirth) {
        return Period.between(dateOfBirth, LocalDate.now()).getYears();
    }
}
