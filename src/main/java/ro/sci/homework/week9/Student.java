package ro.sci.homework.week9;

import java.time.LocalDate;

public class Student {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private String gender;
    private String cnp;

    //Check validating conditions for firstName, lastName, gender and age of a student to can be created
    public Student(String firstName, String lastName, LocalDate dateOfBirth,
                   String gender, String cnp) throws CustomValidationException {
        if (firstName == null || firstName.trim().equals("")) {
            throw new CustomValidationException("First name is empty");
        }

        if (lastName == null || lastName.trim().equals("")) {
            throw new CustomValidationException("Last name is empty");
        }

        if (!gender.equalsIgnoreCase("male") && !gender.equalsIgnoreCase("female")) {
            throw new CustomValidationException("Invalid gender entered, please enter female/FEMALE or male/MALE");
        }

        if (!validateAge(dateOfBirth)) {
            throw new CustomValidationException("Invalid date of birth entered, age must be at least 18 and under 122");
        }

        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender.toLowerCase();
        this.cnp = cnp;
    }

    //Check if a person is too old or too young to pass the conditions
    private boolean validateAge(LocalDate dateOfBirth) {
        if (dateOfBirth.getYear() < 1900) {
            return false;
        } else {
            return dateOfBirth.getYear() <= LocalDate.now().getYear() - 18;
        }
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }


    public String getCnp() {
        return cnp;
    }

    @Override
    public String toString() {
        return "Student{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", gender='" + gender + '\'' +
                ", cnp='" + cnp + '\'' +
                '}';
    }
}
