package ro.sci.homework.week5;

import java.util.ArrayList;
import java.util.List;

public class Hobby {
//  final attributes
    private final String HOBBY_NAME;
    private final int FREQUENCY;

//  configurable attributes
    private List<Address> addresses = new ArrayList<>();
    private List<Country> countries = new ArrayList<>();

//  constructor to initialise our final attributes when make an instance of Hobby
    public Hobby(String hobbyName, int frequency) {
        this.HOBBY_NAME = hobbyName;
        this.FREQUENCY = frequency;
    }

//  methods to can add a country and an address to our instance
    public void addAddress(Address address) {
        this.addresses.add(address);
    }

    public void addCountry(Country country) {
        this.countries.add(country);
    }

    @Override
    public String toString() {
        return "Hobby{" +
                "HOBBY_NAME='" + HOBBY_NAME + '\'' +
                ", countries=" + countries +
                '}';
    }
}
