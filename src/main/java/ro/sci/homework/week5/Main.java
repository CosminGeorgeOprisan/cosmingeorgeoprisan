package ro.sci.homework.week5;

import java.util.*;

public class Main {

    public static void main(String[] args) {

//      create 3 persons
        Person person1 = new Footballer(25, "Cosmin");
        Person person2 = new Swimmer(15, "Daniel");
        Person person3 = new Cyclist(35, "Bogdan");

//      sort persons by name using a comparator
        Set<Person> personsSortedByName = new TreeSet<>(new NameComparator());
        personsSortedByName.add(person1);
        personsSortedByName.add(person2);
        personsSortedByName.add(person3);
        System.out.println(personsSortedByName);

//      display name and age for every person after sorting
        for (Person person : personsSortedByName) {
            System.out.println("The name of the person is " + person.getNAME() +
                    " and the age of the person is " + person.getAGE());
        }

//      sort persons by age using a comparator
        Set<Person> personSortedByAge = new TreeSet<>(new AgeComparator());
        personSortedByAge.add(person1);
        personSortedByAge.add(person2);
        personSortedByAge.add(person3);
        System.out.println(personSortedByAge);

//      display name and age for  every person after sorting
        for (Person person : personSortedByAge) {
            System.out.println("The name of the person is " + person.getNAME() +
                    " and the age of the person is " + person.getAGE());
        }

//      create addresses and countries for our hobbies
        Address addressForFootballers1 = new Address("Str.Santiago Bernabeu");
        Address addressForFootballers2 = new Address("Old Trafford");

        Address addressForCyclists1 = new Address("Str.Calumniate");
        Address addressForCyclists2 = new Address("Str. Calugareni");

        Address addressForSwimmers1 = new Address("Str.Allianz");
        Address addressForSwimmers2 = new Address("Str.Victoriei");

        Country countryForFootballers1 = new Country("Spain");
        Country countryForFootballers2 = new Country("UK");

        Country countryForCyclists1 = new Country("France");
        Country countryForCyclists2 = new Country("Romania");

        Country countryForSwimmers1 = new Country("Germany");
        Country countryForSwimmers2 = new Country("Italy");

//      create hobbies and add specific addresses and countries for a hobby
        Hobby playFootball = new Hobby("football", 5);
        playFootball.addAddress(addressForFootballers1);
        playFootball.addAddress(addressForFootballers2);
        playFootball.addCountry(countryForFootballers1);
        playFootball.addCountry(countryForFootballers2);

        Hobby ridingABike = new Hobby("cycling", 3);
        ridingABike.addAddress(addressForCyclists1);
        ridingABike.addAddress(addressForCyclists2);
        ridingABike.addCountry(countryForCyclists1);
        ridingABike.addCountry(countryForCyclists2);


        Hobby swim = new Hobby("swimming", 2);
        swim.addAddress(addressForSwimmers1);
        swim.addAddress(addressForSwimmers2);
        swim.addCountry(countryForSwimmers1);
        swim.addCountry(countryForSwimmers2);

//      add  some hobbies to a specific person
        person1.addHobby(playFootball);
        person1.addHobby(swim);

        person2.addHobby(ridingABike);
        person2.addHobby(playFootball);

        person3.addHobby(swim);
        person3.addHobby(ridingABike);

//      create a map with person as key and person hobby as value
        Map<Person, List<Hobby>> personMap = new HashMap<>();
        personMap.put(person1, person1.getHobbies());
        personMap.put(person2, person2.getHobbies());
        personMap.put(person3, person3.getHobbies());

/*      verify if our keys are working properly
//      personMap.put(person1, person3.getHobbies());
        System.out.println(personMap);
*/

//      display for a certain person the names of the hobbies and the countries(no need addresses) where these can be practiced
        List<Hobby> hobby = personMap.get(person1);
        System.out.println("Found: " + hobby.toString());
    }
}
