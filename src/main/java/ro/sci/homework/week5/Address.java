package ro.sci.homework.week5;

public class Address {

    private final String ADDRESS_NAME;

    public Address(String addressName) {
        this.ADDRESS_NAME = addressName;
    }

    @Override
    public String toString() {
        return "Address{" +
                "addressName='" + ADDRESS_NAME + '\'' +
                ", countryName='";
    }
}
