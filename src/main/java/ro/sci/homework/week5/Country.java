package ro.sci.homework.week5;

public class Country {

    private final String COUNTRY_NAME;

    public Country(String COUNTRY_NAME) {
        this.COUNTRY_NAME = COUNTRY_NAME;
    }

    @Override
    public String toString() {
        return "Country{" +
                "COUNTRY_NAME='" + COUNTRY_NAME + '\'' +
                '}';
    }
}
