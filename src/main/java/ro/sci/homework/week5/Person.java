package ro.sci.homework.week5;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Person {
//  final attributes
    private final int AGE;
    private final String NAME;

//  configurable attributes
    private List<Hobby> hobbies = new ArrayList<>();

//  constructor to initialise our final attributes when make an instance of Person
    public Person(int AGE, String NAME) {
        this.AGE = AGE;
        this.NAME = NAME;
    }

//  getters for our attributes
    public List<Hobby> getHobbies() {
        return hobbies;
    }

    public int getAGE() {
        return AGE;
    }

    public String getNAME() {
        return NAME;
    }

//  method to can add a hobby to our instance
    public void addHobby(Hobby hobby) {
        this.hobbies.add(hobby);
    }

//  overriding equal and hash method to verify only the age and name of a person without class and lists
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null){
            return false;
        }
        final var person = (Person) o;
        return AGE == person.AGE && Objects.equals(NAME, person.NAME);
    }

    @Override
    public int hashCode() {
        return Objects.hash(AGE, NAME);
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + AGE +
                ", name='" + NAME + '\'' +
                '}';
    }
}
