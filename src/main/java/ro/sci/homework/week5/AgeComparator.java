package ro.sci.homework.week5;

import java.util.Comparator;

public class AgeComparator implements Comparator<Person> {

//  sorting by age in ascending order
    @Override
    public int compare(Person o1, Person o2) {
        return Integer.compare(o1.getAGE(), o2.getAGE());
    }
}
