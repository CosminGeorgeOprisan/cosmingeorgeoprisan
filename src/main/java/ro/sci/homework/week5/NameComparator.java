package ro.sci.homework.week5;

import java.util.Comparator;

public class NameComparator implements Comparator<Person> {

//  sorting by name in descending order
    @Override
    public int compare(Person o1, Person o2) {
        return o2.getNAME().compareTo(o1.getNAME());
    }
}
