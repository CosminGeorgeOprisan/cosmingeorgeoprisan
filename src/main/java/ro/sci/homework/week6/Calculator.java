package ro.sci.homework.week6;

import java.util.HashMap;
import java.util.Map;

public class Calculator {

    //  Create a map with measureUnits as keys and powers of 10 for every key compared with the smallest one as values
    private Map<String, Integer> measureUnitsMap = new HashMap<String, Integer>();

    public Calculator() {
        measureUnitsMap.put("mm", 0);
        measureUnitsMap.put("cm", 1);
        measureUnitsMap.put("dm", 2);
        measureUnitsMap.put("m", 3);
        measureUnitsMap.put("km", 6);
    }

    //  convertUnit method converts a measureUnit to another one based on the values returned when we call a key
    //  ex: if we want to convert 2 kms in meters we need to multiply 2 * Math.pow(10, (6 - 3))
    private Double convertUnit(Double number, String fromMeasureUnit, String toMeasureUnit) {

        return number * Math.pow(10, (measureUnitsMap.get(fromMeasureUnit) - measureUnitsMap.get(toMeasureUnit)));
    }



    public String calculate(String expression, String toMeasureUnit) {

        String returnedMessage = "";
        Double result = 0.0;

        // verify if our expression contains only valid operators
        if ((expression.contains("+") || expression.contains("-")) &&
                !(expression.contains("*") || expression.contains("/"))) {

            // if we pass that check we can split our expression in an array of substrings
            String[] stringsArray = expression.split(" ");

            // initialize the result with the first number from the expression,we will also store results obtained inside
            result = convertUnit(Double.parseDouble(stringsArray[0]), stringsArray[1], toMeasureUnit);

            // we need to search if more operands & operators exists and if exists to make the sum/subtraction between them
            for (int i = 2; i < stringsArray.length; i++) {
                if (stringsArray[i].equals("+")) {
                    result += convertUnit(Double.parseDouble(stringsArray[i + 1]), stringsArray[i + 2], toMeasureUnit);
                } else if (stringsArray[i].equals("-")) {
                    result -= convertUnit(Double.parseDouble(stringsArray[i + 1]), stringsArray[i + 2], toMeasureUnit);
                }
            }
            //  When the array is finished we will return the result obtained in the measure unit specified
            returnedMessage = returnedMessage.concat(result + " " + toMeasureUnit);
        } else {

            // If we find an invalid operator will return an error message to the user
            returnedMessage = "Found invalid operands in the expression";
        }
        return returnedMessage;
    }

}


