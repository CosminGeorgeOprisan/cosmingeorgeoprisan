package ro.sci.homework.week6;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // create an instance object of Calculator class
        Calculator calculator = new Calculator();

        // Create a scanner object to can ask the user about his desired measure unit
        Scanner scanner = new Scanner(System.in);
        System.out.print("Insert desired unit: ");
        String measureUnit = scanner.next();

        // Calculate the expression in the desired measure unit
        System.out.print(calculator.calculate("7 m - 2 dm + 6 mm + 3 cm", measureUnit));
    }
}
