package ro.sci.homework.week10;
import lombok.*;
import java.time.LocalDate;

@Builder
@Data
@Getter
@Setter

public class Person {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;


    public static Person lineToPerson(String line) {
        String[] fields = line.split(",");

        return Person.builder()
                .firstName(fields[0])
                .lastName(fields[1])
                .dateOfBirth(reformatLocalDate(fields[2]))
                .build();
    }

    public static LocalDate reformatLocalDate(String date) {
        String[] time = date.split("-");
        return LocalDate.of(Integer.parseInt(time[0]), Integer.parseInt(time[1]), Integer.parseInt(time[2]));
    }

    @Override
    public String toString() {
        return firstName + " " + lastName + " " + dateOfBirth;
    }

}
