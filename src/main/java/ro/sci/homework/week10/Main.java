package ro.sci.homework.week10;

import java.io.*;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {

        PersonRepo personRepo = new PersonRepo();

        System.out.println("The list of persons is:");
        List list = personRepo.readFromFileToList("in.txt");

        System.out.println("Persons who passed the data processing:");
        String myString = personRepo.processingData(12, list);
        personRepo.writeData("out.txt", myString);

    }
}
