package ro.sci.homework.week10;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class PersonRepo {

    private static List<Person> personList = new ArrayList<>();

    //Read information from our input file,save them in form of a person object and add to our list of persons
    public  List readFromFileToList(String inputFilePath) throws IOException {

        try (BufferedReader reader = new BufferedReader(new FileReader(inputFilePath))) {
            String line;
            while ((line = reader.readLine()) != null && line.length() > 0) {
                Person person = Person.lineToPerson(line);
                personList.add(person);
            }
        }
        System.out.println(personList);
        return personList;
    }

    //Process the list obtained through the stream and see who pass the filter of the target month displaying the result as String
    public String processingData(int targetMonth, List<Person> personList) {
        String myString = personList.stream()
                .filter(person -> person.getDateOfBirth().getMonth().getValue() == targetMonth)
                .sorted(Comparator.comparing(Person::getFirstName))
                .map(person -> person.getFirstName() + " " + person.getLastName() + ", ")
                .reduce("", (n1, n2) -> n1 + n2);
        System.out.println(myString);
        return myString;
    }

    //Write the result in our output file
    public void writeData(String pathName,String myString) {
        File destination = new File(pathName);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(destination))){
            writer.write(myString);
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
