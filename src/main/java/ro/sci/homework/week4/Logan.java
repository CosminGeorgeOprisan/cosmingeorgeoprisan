package ro.sci.homework.week4;

public class Logan extends Dacia {
    public Logan(float availableFuel, String chassisNumber) {
        super(45, "Gasoline", 5, 5.2f);
        setTireSize(14);

        //can not have more fuel than the fuel tank size
        if (availableFuel <= super.getFuelTankSize()) {
            super.setAvailableFuel(availableFuel);
        } else {
            System.out.println("Can not have more fuel than tank capacity which is: " + super.getFuelTankSize());
        }

        super.setChassisNumber(chassisNumber);
    }
}
