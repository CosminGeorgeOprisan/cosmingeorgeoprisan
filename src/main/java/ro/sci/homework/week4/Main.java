package ro.sci.homework.week4;

public class Main {

    public static void main(String[] args) {
//          Checks
//        Car car = new Car();
//        Car car = new Dacia();

// create a cycle
          Car car = new Duster(30f, "chassisNumber1");
          car.start();
          car.shiftGear(1);
          car.drive(0.1f);
          car.shiftGear(2);
          car.drive(0.2f);
          car.shiftGear(3);
          car.drive(0.5f);
          car.shiftGear(4);
          car.drive(0.5f);
          car.shiftGear(6);
          car.drive(10);
          car.shiftGear(4);
          car.drive(0.5f);
          car.shiftGear(3);
          car.stop();

//       Obtain results
         float availableFuel = car.getAvailableFuel();
         float fuelConsumedPer100Km = car.getAverageFuelConsumption();

//      print results
        System.out.println("Available fuel for Duster is: " + availableFuel);
        System.out.println("Average consumption for Duster is: " + fuelConsumedPer100Km);



    }
}
