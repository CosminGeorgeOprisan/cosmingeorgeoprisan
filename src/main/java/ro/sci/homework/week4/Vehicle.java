package ro.sci.homework.week4;

public interface Vehicle {
    public void start();
    public void stop();
    public void drive(float km);
}
