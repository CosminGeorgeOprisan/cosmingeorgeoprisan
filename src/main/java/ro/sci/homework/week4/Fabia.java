package ro.sci.homework.week4;

public class Fabia extends Skoda {
    public Fabia(float availableFuel, String chassisNumber) {
        super(53, "GPL", 5, 4.1f);
        setTireSize(13);

        //can not have more fuel than the fuel tank size
        if (availableFuel <= super.getFuelTankSize()) {
            super.setAvailableFuel(availableFuel);
        } else {
            System.out.println("Can not have more fuel than tank capacity which is: " + super.getFuelTankSize());
        }

        super.setChassisNumber(chassisNumber);
    }
}
