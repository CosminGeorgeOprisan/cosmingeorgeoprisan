package ro.sci.homework.week4;

public abstract class Skoda extends Car {

    private float availableFuel;
    private String chassisNumber;

    public Skoda(int fuelTankSize, String fuelType, int maxGears, float consumptionPer100Km) {
        super(fuelTankSize, fuelType, maxGears, consumptionPer100Km);
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public float getAvailableFuel() {
        return availableFuel - super.getCurrentConsumption();
    }

    public void setAvailableFuel(float availableFuel) {
        this.availableFuel = availableFuel;
    }

    public float getAverageFuelConsumption() {
        int Percent = 100;

        float avgFuelConsumption = (super.getCurrentConsumption() * Percent) / super.getCurrentDrivenKms();
        return avgFuelConsumption;
    }
}
