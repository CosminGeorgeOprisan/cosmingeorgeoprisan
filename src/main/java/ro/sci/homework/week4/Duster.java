package ro.sci.homework.week4;

public class Duster extends Dacia{
    public Duster(float availableFuel, String chassisNumber) {
        super(50, "Diesel", 6, 6.3f);
        setTireSize(16);

        //can not have more fuel than the fuel tank size
        if (availableFuel <= super.getFuelTankSize()) {
            super.setAvailableFuel(availableFuel);
        } else {
            System.out.println("Can not have more fuel than tank capacity which is: " + super.getFuelTankSize());
        }

        super.setChassisNumber(chassisNumber);
    }

}
