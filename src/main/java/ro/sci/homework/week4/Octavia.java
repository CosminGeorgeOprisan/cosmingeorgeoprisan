package ro.sci.homework.week4;

public class Octavia extends Skoda{
    public Octavia(float availableFuel, String chassisNumber) {
        super(35, "Diesel", 6, 4.5f);
        setTireSize(17);

        //can not have more fuel than the fuel tank size
        if (availableFuel <= super.getFuelTankSize()) {
            super.setAvailableFuel(availableFuel);
        } else {
            System.out.println("Can not have more fuel than tank capacity which is: " + super.getFuelTankSize());
        }

        super.setChassisNumber(chassisNumber);
    }
}
