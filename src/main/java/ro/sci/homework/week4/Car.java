package ro.sci.homework.week4;

public abstract class Car implements Vehicle {
    //  final attributes
    private final int fuelTankSize;
    private final String fuelType;
    private final int maxGears;
    private final float consumptionPer100Km;


    //  configurable attributes
    private int tireSize;
    private float currentConsumption; //consumption during a driving cycle
    private float currentDrivenKms; //kms made in a driving cycle
    private float currentGear;
    private Boolean isEngineStarted = false;
    private Boolean isEngineStopped = false;

    //  constructor
    public Car(int fuelTankSize, String fuelType, int maxGears, float consumptionPer100Km) {
        this.fuelTankSize = fuelTankSize;
        this.fuelType = fuelType;
        this.maxGears = maxGears;
        this.consumptionPer100Km = consumptionPer100Km;
    }

    //  abstract methods
    public abstract float getAvailableFuel();

    public abstract float getAverageFuelConsumption();

    //  getters and setters needed
    public int getFuelTankSize() {
        return fuelTankSize;
    }

    public int getMaxGears() {
        return maxGears;
    }

    public float getConsumptionPer100Km() {
        return consumptionPer100Km;
    }

    public int getTireSize() {
        return tireSize;
    }

    public void setTireSize(int tireSize) {
        this.tireSize = tireSize;
    }

    public float getCurrentConsumption() {
        return currentConsumption;
    }

    public void setCurrentConsumption(float currentConsumption) {
        this.currentConsumption = currentConsumption;
    }

    public float getCurrentGear() {
        return currentGear;
    }

    public void setCurrentGear(float currentGear) {
        this.currentGear = currentGear;
    }

    public float getCurrentDrivenKms() {
        return currentDrivenKms;
    }

    public void setCurrentDrivenKms(float distance) {
        this.currentDrivenKms = distance;
    }

    //  Behaviors
    public void shiftGear(int gear) {
        if (gear <= getMaxGears()) {
            setCurrentGear(gear);
        } else {
            throw new IllegalArgumentException("This car has not this " + gear);
        }
    }

    public void calculateConsumption(double kms) {
        double temporarilyCurrentGear = getCurrentGear();
        double temporarilyMaxGear = getMaxGears();
        float temporarilyConsumptionPer100Km;
        float temporarilyConsumedFuel;
        int Percent = 100;
        double baseForPow = 1.3;

        //  Calculate consumption per 100 km for current gear
        temporarilyConsumptionPer100Km = (float) (getConsumptionPer100Km() *
                Math.pow(baseForPow, (temporarilyMaxGear - temporarilyCurrentGear)));

        //calculate how much fuel was consumed during current drive in current gear;
        temporarilyConsumedFuel = (float) ((temporarilyConsumptionPer100Km * kms) / Percent);

        setCurrentConsumption(temporarilyConsumedFuel);
    }

// verify if our car begins is not already started when reset the stats
    @Override
    public void start() {
        if (isEngineStarted == true) {
            throw new IllegalArgumentException("The car started already");
        } else {
            isEngineStarted = true;
            isEngineStopped = false;
            currentConsumption = 0.0f;
            currentDrivenKms = 0.0f;
            System.out.println();
        }
    }
    @Override
    public void drive(float km) {
        if (isEngineStarted == true) {
            float distance = getCurrentDrivenKms();
            distance += km;
//          update total distance made in the current  cycle
            setCurrentDrivenKms(distance);
//          calculate fuel consumption for the distance made in this current gear
            calculateConsumption(km);
        } else {
            System.out.println("The car is stopped ");
        }
    }
    @Override
    public void stop(){
        isEngineStarted = false;
        isEngineStopped = true;
        System.out.println("The car is stopped");
    }
}