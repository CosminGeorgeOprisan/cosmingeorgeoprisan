package ro.sci.homework.week11;

import java.util.EnumMap;
import java.util.Set;

public class FestivalStatisticsThread extends Thread {

    private FestivalGate gate;

    public FestivalStatisticsThread(FestivalGate gate) {
        this.gate = gate;
    }

    //Show details about the numbers of people with a specific ticketType entered and the total numbers of persons entered
    //Break it if we reach to the maximum number permitted
    @Override
    public void run() {
        boolean was500already = false;
        while (true) {

            var attendeesQueue = this.gate.getAttendeesQueue();

            if (attendeesQueue.isEmpty()) {
                continue;
            }
            if (was500already) {
                break;
            }
            if (attendeesQueue.size() == 500) {
                was500already = true;
            }

            EnumMap<TicketType, Integer> report = new EnumMap<>(TicketType.class);
            for (TicketType ticketType : attendeesQueue) {
                if (report.containsKey(ticketType)) {
                    report.put(ticketType, report.get(ticketType) + 1);
                } else {
                    report.put(ticketType, 1);
                }
            }

            Set<TicketType> ticketTypes = report.keySet();
            System.out.println(report.values().stream().mapToInt(v -> v).sum() + " people have entered");
            for (TicketType type : ticketTypes) {
                System.out.println(report.get(type) + " people have " + type.name() + " ticket type");
            }

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
