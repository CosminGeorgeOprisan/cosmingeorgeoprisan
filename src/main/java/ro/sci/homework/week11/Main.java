package ro.sci.homework.week11;

public class Main {

    public static void main(String[] args) {

        FestivalGate gate = new FestivalGate();
        FestivalStatisticsThread statisticsThread = new FestivalStatisticsThread(gate);
        statisticsThread.start();

        for (int i = 0; i < 500; i++) {
            TicketType random = TicketType.randomTicket();
            FestivalAttendeeThread festivalAttendeeThread = new FestivalAttendeeThread(random, gate);
            Thread t = new Thread(festivalAttendeeThread);
            t.start();
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
