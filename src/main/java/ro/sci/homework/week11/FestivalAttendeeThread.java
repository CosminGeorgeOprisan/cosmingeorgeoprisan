package ro.sci.homework.week11;

public class FestivalAttendeeThread extends Thread {
    private TicketType ticketType;
    private FestivalGate gate;

    public FestivalAttendeeThread(TicketType ticketType, FestivalGate festivalGate) {
        this.ticketType = ticketType;
        this.gate = festivalGate;
    }

    //add  values of ticketsType into the queue
    @Override
    public void run() {
        try {
            Thread.sleep(600);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.gate.getAttendeesQueue().add(ticketType);
        System.out.println("added " + ticketType.name());
    }
}
