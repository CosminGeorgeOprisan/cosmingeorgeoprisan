package ro.sci.homework.week11;

import java.util.Random;

public enum TicketType {
    FULL,
    FULL_VIP,
    FREE_PASS,
    ONE_DAY,
    ONE_DAY_VIP;

    //Generates a random enum value based on the index got from our index range obtained
    public static TicketType randomTicket() {

        TicketType[] values = TicketType.values();
        int length = values.length;
        int randomIndex = new Random().nextInt(length);
        return values[randomIndex];
    }
}
