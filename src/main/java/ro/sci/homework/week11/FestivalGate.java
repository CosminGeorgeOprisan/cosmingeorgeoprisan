package ro.sci.homework.week11;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;

import lombok.Data;

@Data
public class FestivalGate {

    private Queue<TicketType> attendeesQueue = new ConcurrentLinkedDeque<>();
}
