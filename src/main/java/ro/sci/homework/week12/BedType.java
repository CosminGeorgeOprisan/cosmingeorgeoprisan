package ro.sci.homework.week12;

public enum BedType {

    QUEEN_SIZE{
        @Override
        public String toString() {
            return "Queen Size";
        }
    },

    KING_SIZE{
        @Override
        public String toString() {
            return "King Size";
        }
    },

    STANDARD_ONE_PERSON{
        @Override
        public String toString() {
            return "Standard Size";
        }
    };

    public static BedType getRandom() {
        return values()[(int) (Math.random() * values().length)];
    }
}
