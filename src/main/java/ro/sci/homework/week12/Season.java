package ro.sci.homework.week12;

public enum Season {
    SPRING,
    SUMMER,
    AUTUMN,
    WINTER;

    public static Season getRandom() {
        return values()[(int) (Math.random() * values().length)];
    }
}
