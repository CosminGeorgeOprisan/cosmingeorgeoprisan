package ro.sci.homework.week12;

public class Main {

    public static void main(String[] args) {

        AccommodationTable accommodationTable = new AccommodationTable();
        RoomFairTable roomFairTable = new RoomFairTable();
        AccommodationRoomFairRelation accommodationFairRelation = new AccommodationRoomFairRelation();

        accommodationTable.start();
        roomFairTable.start();
        accommodationFairRelation.start();
    }
}
