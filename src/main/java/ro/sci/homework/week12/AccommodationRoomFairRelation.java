package ro.sci.homework.week12;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AccommodationRoomFairRelation extends Thread {

    //fill accommodation_room_fair_relation table
    public void run() {
        try {
            sleep(1000);
            try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Homework",
                    "postgres", "Eminem123@")) {
                PreparedStatement preparedStatement3 = connection.prepareStatement("INSERT INTO accommodation_room_fair_relation "
                        + "(id, accommodation_id, room_fair_id) values (?,?,?)");

                for (int i = 1; i <= 10; i++) {
                    preparedStatement3.setInt(1, i);
                    preparedStatement3.setInt(2, i);
                    preparedStatement3.setInt(3, i);
                    preparedStatement3.executeUpdate();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
