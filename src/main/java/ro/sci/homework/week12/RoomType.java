package ro.sci.homework.week12;

public enum RoomType {

    STANDARD_SINGLE_ROOM {
        @Override
        public String toString() {
            return "Standard Single-Room";
        }
    },

    STANDARD_DOUBLE_ROOM {
        @Override
        public String toString() {
            return "Premium Double-Room";
        }
    },
    PREMIUM_DOUBLE_ROOM {
        @Override
        public String toString() {
            return "Premium Double-Room";
        }
    },

    KING_SUITE {
        @Override
        public String toString() {
            return "King suite";
        }
    },

    QUEEN_SUITE {
        @Override
        public String toString() {
            return "Queen Suite";
        }
    },

    FAMILY_SUITE {
        @Override
        public String toString() {
            return "Family Suite";
        }
    };

    public static RoomType getRandom() {
        return values()[(int) (Math.random() * values().length)];
    }
}