package ro.sci.homework.week8;

public enum Medals {

    FIRSTPLACE("gold medal"),
    SECONDPLACE("silver medal"),
    THIRDPLACE("bronze medal");

    private String medal;

    Medals(String medal) {
        this.medal = medal;
    }

    @Override
    public String toString() {
        return medal;
    }
}
