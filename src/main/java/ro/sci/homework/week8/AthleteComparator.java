package ro.sci.homework.week8;

import java.util.Comparator;

public class AthleteComparator implements Comparator<Athlete> {

    @Override
    public int compare(Athlete athlete1, Athlete athlete2) {
        return athlete1.getTime().compareTo(athlete2.getTime());
    }
}
