package ro.sci.homework.week8;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class AthleteRepo {

    private static List<Athlete> athleteList = new ArrayList<>();

    public static List<Athlete> getAthleteList() {
        return athleteList;
    }

    //Read content from a specified file into our list
    public static List readFromFileToList(String fileName) throws IOException {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)))) {
            String myString;
            //Verify to take every line from the file and split whole information in smaller pieces to can work with these
            while ((myString = reader.readLine()) != null) {
                if ((myString.length() > 0)) {
                    String[] athletesArr = myString.split(",");
                    athleteList.add(new Athlete(Integer.parseInt(athletesArr[0]), athletesArr[1], athletesArr[2],
                            athletesArr[3], athletesArr[4], athletesArr[5], athletesArr[6]));
                }
            }
            //Verify all athletes we got from the file
            for (Athlete athlete : athleteList) {
                System.out.println(athlete);
            }
        }
        return athleteList;
    }
    //Verify if we obtained shootingRange series from our file and add penalties for every miss
    private static int missedShots(Athlete athlete) {
        int timeInSeconds = 0;
        String[] firstSeries = athlete.getFirstShootingRange().split("");
        for (String first : firstSeries) {
            if (!(first.equals("o") || first.equals("x")))
                throw new IllegalArgumentException("Could not read first series");
            if (first.equals("o")) timeInSeconds += 10;
        }
        String[] secondSeries = athlete.getSecondShootingRange().split("");
        for (String second : secondSeries) {
            if (!(second.equals("o") || second.equals("x")))
                throw new IllegalArgumentException("Could not read second series");
            if (second.equals("o")) timeInSeconds += 10;
        }
        String[] thirdSeries = athlete.getThirdShootingRange().split("");
        for (String s : thirdSeries) {
            if (!(s.equals("o") || s.equals("x"))) throw new IllegalArgumentException("Could not read third series");
            if (s.equals("o")) timeInSeconds += 10;
        }
        return timeInSeconds;
    }
    //Update the final time result for every athlete after penalties applied
    public static void calculateTime(List<Athlete> athletesList) {
        System.out.println("Final standings:");
        try {
            for (Athlete athlete : athletesList) {
                String[] string = athlete.getTime().split(":");
                int timeInSeconds = Integer.parseInt(string[0]) * 60 + Integer.parseInt(string[1]) + missedShots(athlete);
                int minutes = timeInSeconds / 60;
                int seconds = timeInSeconds % 60;
                String time = minutes + ":" + seconds;
                athlete.setTime(time);
            }
        } catch (NumberFormatException numEx) {
            System.out.println("Incorrect format");
            numEx.printStackTrace();
        }
    }
    // Sort our list using final time result comparator and print standings with medals obtained
    public static Athlete showStandings(List<Athlete> athleteList) {
        athleteList.sort(new AthleteComparator());
        int i = 0;
        while (i <= 2 && i < athleteList.size()) {
            switch (i) {
                case 0:
                    System.out.println(Medals.FIRSTPLACE + " for " + athleteList.get(i));
                    break;
                case 1:
                    System.out.println(Medals.SECONDPLACE + " for " + athleteList.get(i));
                    break;
                case 2:
                    System.out.println(Medals.THIRDPLACE + " for " + athleteList.get(i));
            }

            i++;
        }

        return athleteList.get(0);
    }
}


